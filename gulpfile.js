'use strict';
const path = require('path');
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const autoprefixer = require('autoprefixer');
const colorguard = require('colorguard');
const doiuse = require('doiuse');
const cssnano = require('cssnano');
const stylelint = require('stylelint');
const pkg = require('./package.json');
const syntaxScss = require('postcss-scss');

const scssGlob = "./scss/**/*.scss";

function buildProd() {
    // This build is minified, and don't embed sourcemap
    return gulp.src(pkg.main)
        .pipe(sass({ outputStyle: "expanded" }).on('error', sass.logError))
        .pipe(postcss([
            cssnano({
                preset: [
                    'default',
                    {
                        calc: {
                            preserve: true
                        },
                        discardComments: {
                            removeAll: true,
                        },
                    },
                ]
            }),
            autoprefixer({grid: 'no-autoplace'}),
        ]))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest(path.dirname(pkg.style)));

}
function buildDev() {
    // This build is not minified, but embed sourcemap
    return gulp.src(pkg.main)
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: "expanded" }).on('error', sass.logError))
        .pipe(postcss([
            autoprefixer({grid: 'no-autoplace'}),
        ]))
        .pipe(sourcemaps.write(path.dirname('.')))
        .pipe(gulp.dest(path.dirname(pkg.style)));

}

function check() {
    return gulp.src(scssGlob)
        .pipe(sass({ outputStyle: "expanded" }).on('error', sass.logError))
        .pipe(postcss([
            stylelint(),
            colorguard(),
            doiuse({
                ignore: [
                    'border-radius', // This is not really important if we don't have border-radius
                ],
            }),
        ], { syntax: syntaxScss }));
}

function watch() {
    const toRun = gulp.parallel(build, check);
    gulp.parallel(build, check)();
    return gulp.watch(scssGlob, toRun);
}

exports.watch = watch;
exports.check = check;
exports.build = gulp.parallel(buildDev, buildProd);
exports['build:prod'] = buildProd;
exports['build:dev'] = buildDev;
exports.default = exports.build;
