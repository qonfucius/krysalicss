# Krysalicss

Krysalicss is a modern and modular CSS framework, using latest module system from sass and css variables.

_ Not_ usable in production yet

## About 0.0.x versions

Versions below 0.1.0 are made only by technical guys, not yet reviewed by a designer. 
So, default colors, default styles will change from a version to the next, 
and potentially break a lot of things for developers who are using this library. 

Please upgrade with caution, and use exact version match.

## Goals

* Be highly configurable
* Be able to have native dark-mode theming
* Be modular
* Be intrusive and not intrusive (configurable), to generate lighter CSS when using web components.
* Be battery-included
* Be written in Scss
* Be Javascript free

## Wall of fame

This framework is highly inspired by some opensources projects, for spirit, class naming, and other things.
Thanks to them.
- [Bulma](https://bulma.io/) ([MIT Licence](https://github.com/jgthms/bulma/blob/master/LICENSE))
- [Picnic CSS](https://picnicss.com/) ([MIT Licence](https://github.com/franciscop/picnic/blob/master/LICENSE))